import json
import logging

import werkzeug.wrappers

from odoo import http
from odoo.addons.restful.common import invalid_response, valid_response
from odoo.exceptions import AccessDenied, AccessError
from odoo.http import request

_logger = logging.getLogger(__name__)
expires_in = 'restful.access_token_expires_in'

class AccessToken(http.Controller):
    """."""

    def __init__(self):
    
        self._token = request.env['api.access_token']
        self._expires_in = request.env.ref(expires_in).sudo().value
 
    @http.route('/api/auth/token', methods=['GET','POST'], type='http', auth='none', csrf=False)
    def token(self, **post):
        """The token URL to be used for getting the access_token:

        Args:
            **post must contain login and password.
        Returns:

            returns https response code 404 if failed error message in the body in json format
            and status code 202 if successful with the access_token.
        Example:
           import requests

           headers = {'content-type': 'text/plain', 'charset':'utf-8'}

           data = {
               'login': 'admin',
               'password': 'admin',
               'db': 'galago.ng'
            }
           base_url = 'http://odoo.ng'
           eq = requests.post(
               '{}/api/auth/token'.format(base_url), data=data, headers=headers)
           content = json.loads(req.content.decode('utf-8'))
           headers.update(access-token=content.get('access_token'))
        """
        _token = request.env['api.access_token']
        # params = ['db', 'login', 'password']
        # params = {key: post.get(key) for key in params if post.get(key)}
        db = post.get('db') if 'db' in post else request.session.db 
        username = post.get('login'), 
        password =  post.get('password')
        
        if not all([db, username, password]):
            # Empty 'db' or 'username' or 'password:
            return invalid_response(400, 'missing error', 'either of the following are missing [db, username,password]')
        # Login in odoo database:
        try:
            request.session.authenticate(db, username, password)
        except Exception as e:
            # Invalid database:
            info = "The database name is not valid {}".format((e))
            error = 'invalid_database'
            _logger.error(info)
            return invalid_response(400, error, info)

        uid = request.session.uid
        # odoo login failed:
        if not uid:
            info = "authentication failed"
            error = 'authentication failed'
            _logger.error(info)
            return invalid_response(401, error, info)

        # Generate tokens
        access_token = _token.find_one_or_create_token(
            user_id=uid, create=True)
        # Successful response:
        return werkzeug.wrappers.Response(
            status=200,
            content_type='application/json; charset=utf-8',
            headers=[('Cache-Control', 'no-store'),
                     ('Pragma', 'no-cache')],
            response=json.dumps({
                # 'uid': uid,
                # 'user_context': request.session.get_context() if uid else {},
                "token_type":"bearer",
                # 'company_id': request.env.user.company_id.id if uid else None,
                'access_token': access_token,
                'expires_in': self._expires_in,
            }),
        )


    @http.route(["/api/auth/token"], methods=["DELETE"], type="http", auth="none", csrf=False)
    def delete(self, **post):
        """Delete a given token"""
        token = request.env["api.access_token"]
        access_token = post.get("access_token")

        access_token = token.search([("token", "=", access_token)], limit=1)
        if not access_token:
            error = "Access token is missing in the request header or invalid token was provided"
            return invalid_response(400, error)
        for token in access_token:
            token.unlink()
        # Successful response:
        return valid_response([{"message": "access token %s successfully deleted" % (access_token,), "delete": True}])
