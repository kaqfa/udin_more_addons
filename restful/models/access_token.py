import hashlib
import logging
import string
import random
from odoo import models, fields, api
from datetime import datetime, timedelta

from odoo import api, fields, models
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)

expires_in = 'restful.access_token_expires_in'

def nonce(length=40):
    # rbytes = os.urandom(length)
    access_token  = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(40)])
    return access_token


class APIAccessToken(models.Model):
    _name = "api.access_token"
    _description = "API Access Token"

    token = fields.Char("Access Token", required=True)
    user_id = fields.Many2one("res.users", string="User", required=True) 
    expires = fields.Datetime('Expires', required=True)
    scope = fields.Char(string="Scope")

    def find_one_or_create_token(self, user_id=None, create=False):
        if not user_id:
            user_id = self.env.user.id
 
        access_token = self.env["api.access_token"]
        if create:
            expires = datetime.now() + \
                timedelta(seconds=int(self.env.ref(expires_in).sudo().value))
            vals = {
                "user_id": user_id,
                "scope": "userinfo",
                'expires': expires.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                "token": nonce(50),
            }
            access_token = self.env["api.access_token"].sudo().create(vals)
        else:
            access_token = self.env["api.access_token"].sudo().search([("user_id", "=", user_id)], order="id DESC", limit=1)
            if access_token:
                access_token = access_token[0]
            if access_token.has_expired():
                access_token = None
            
        if not access_token:
            return None
        return access_token.token

    def _allow_scopes(self, scopes):
        self.ensure_one()
        if not scopes:
            return True

        provided_scopes = set(self.scope.split())
        resource_scopes = set(scopes)

        return resource_scopes.issubset(provided_scopes)

    @api.multi
    def has_expired(self):
        self.ensure_one()
        return datetime.now() > fields.Datetime.from_string(self.expires)

class Users(models.Model):
    _inherit = "res.users"
    token_ids = fields.One2many("api.access_token", "user_id", string="Access Tokens")
